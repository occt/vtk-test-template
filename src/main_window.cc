#include <QDesktopWidget>
#include <QVBoxLayout>
#include <QVTKOpenGLNativeWidget.h>
#include <iostream>
#include <qapplication.h>
#include <qcoreevent.h>
#include <qmainwindow.h>
#include <qpoint.h>
#include <qwidget.h>

#include "main_window.h"
#include "vtk_include.h"

VTK_MODULE_INIT(vtkRenderingOpenGL2) // otherwise I received error: no override found for ...
VTK_MODULE_INIT(vtkInteractionStyle)

MainWindow::MainWindow()
    : QMainWindow(nullptr)
    , m_widget(this) {
  m_widget.setFormat(QVTKOpenGLNativeWidget::defaultFormat());
  setCentralWidget(&m_widget);

  m_render_wnd = m_widget.renderWindow();
  m_renderer = vtkSmartPointer<vtkRenderer>::New();
  m_interactor = m_render_wnd->GetInteractor();
  m_render_wnd->AddRenderer(m_renderer);

  auto colors = vtkSmartPointer<vtkNamedColors>::New();
  m_renderer->SetBackground(colors->GetColor3d("Beige").GetData());
  m_renderer->ResetCamera();
  m_renderer->GetActiveCamera()->Elevation(60.0);
  m_renderer->GetActiveCamera()->Azimuth(30.0);
  m_renderer->GetActiveCamera()->Dolly(1.2);
}

void MainWindow::update2() {
  m_renderer->ResetCamera();
  m_renderer->GetActiveCamera()->Azimuth(-10);
  m_renderer->GetActiveCamera()->Elevation(-20);

  m_renderer->GetRenderWindow()->Render();
  m_widget.update();

  this->update();
}

void MainWindow::setCenter() {
  const auto w = QApplication::desktop()->width();
  const auto h = QApplication::desktop()->height();
  QPoint pt((w - this->width()) / 2, (h - this->height()) / 2);
  this->move(pt);
}

bool MainWindow::event(QEvent *event) {
  auto ret = QWidget::event(event);
  if (event->type() == QEvent::Polish) {
  }
  return ret;
}

void MainWindow::showEvent(QShowEvent *event) { //
  QMainWindow::showEvent(event);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
  QMainWindow::resizeEvent(event);
}

void MainWindow::plotCylinder() {
  auto cylinder = vtkSmartPointer<vtkCylinderSource>::New();
  cylinder->SetResolution(256);

  auto cylinderMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  cylinderMapper->SetInputConnection(cylinder->GetOutputPort());

  auto cylinderActor = vtkSmartPointer<vtkActor>::New();
  cylinderActor->SetMapper(cylinderMapper);
  cylinderActor->GetProperty()->SetColor(1.0000, 0.3883, 0.2784);
  cylinderActor->RotateX(30.0);
  cylinderActor->RotateY(-45.0);

  m_renderer->AddActor(cylinderActor);
  m_renderer->ResetCamera();
  m_renderer->GetActiveCamera()->Zoom(1.5);
}