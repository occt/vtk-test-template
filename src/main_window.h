#pragma once

#include <vtkSmartPointer.h>

#include <QMainWindow>
#include <QVTKOpenGLNativeWidget.h>
#include <QtWidgets/QMainWindow>
#include <qcoreevent.h>
#include <qevent.h>

class vtkRenderWindowInteractor;
// class QVTKOpenGLNativeWidget;
class vtkRenderWindow;
class vtkRenderer;

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow();
  ~MainWindow() = default;

  void update2();
  void setCenter();
  void plotCylinder();

protected:
  void resizeEvent(QResizeEvent *event) override;
  void showEvent(QShowEvent *event) override;
  bool event(QEvent *event) override;

public:
  vtkSmartPointer<vtkRenderWindowInteractor> m_interactor;
  QVTKOpenGLNativeWidget m_widget;
  vtkSmartPointer<vtkRenderWindow> m_render_wnd;
  vtkSmartPointer<vtkRenderer> m_renderer;

  // vtkNew<vtkNamedColors> colors;
};