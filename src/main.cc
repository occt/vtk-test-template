#include <QApplication>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QVTKOpenGLNativeWidget.h>

#include "main_window.h"
#include "vtkType.h"
#include "vtk_inc.h"

int main(int argc, char *argv[]) {
  // setenv("MESA_GL_VERSION_OVERRIDE", "3.2", 1);
  auto colors = vtkSmartPointer<vtkNamedColors>::New();
  QApplication app(argc, argv);
  MainWindow mainWindow;

  mainWindow.show();
  mainWindow.resize(1024, 768);
  mainWindow.setCenter();

  mainWindow.plotCylinder();
  mainWindow.update2();

  return app.exec();
}